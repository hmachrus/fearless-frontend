window.addEventListener('DOMContentLoaded', async () => {
    function createCard(name, description, pictureUrl, dates, place) {
        return `
          <div class="card shadow-sm p-3 mb-5 bg-white rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <p class="card-text">${description}</p>
              <small class="text-muted">${place}</small>
              <div class="card-footer">
                <small class="text-muted">
                ${dates}
                </small>
              </div>
            </div>

          </div>
        `;
      }


        const url = 'http://localhost:8000/api/conferences/';

        try {
          const response = await fetch(url);

          if (!response.ok) {
            // Figure out what to do when the response is bad
          } else {
            const data = await response.json();
            let index = 0;
            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const place = details.conference.location.name;
                var startDate = new Date(details.conference.starts);
                startDate = startDate.toLocaleDateString();
                var endDate = new Date(details.conference.ends);
                endDate = endDate.toLocaleDateString();
                const dates = `${startDate} - ${endDate}`;
                const html = createCard(title, description, pictureUrl, dates, place);
                const column = document.querySelector(`#col-${index % 3}`);
                column.innerHTML += html;
                index += 1;
              }
            }

          }
        } catch (e) {
          console.error(e);
          // Figure out what to do if an error is raised
        }

      });
